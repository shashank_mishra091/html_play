<?php 
error_reporting(E_ALL);
ini_set('display_errors', '1');
require 'helper.php';
require 'aes_helper.php';
    
$base_url=$_SERVER['HTTP_X_FORWARDED_PROTO'].'://'.$_SERVER['HTTP_HOST'];
$url = $base_url . "/data_model/fanwall/Fan_wall/on_request_create_video_link_v3/1";

//$_POST['token']= urldecode(base64_decode($_POST['token']));

$document = array('file_url' => $url, 'name' => $_POST['token']);
$data = file_curl_contents($document);

if($data['status']==1){
    $meta=explode('_',$_POST['token']);
    $token=end($meta);
    $c_url=$data['data']['link'];
    $decoded=aes_cbc_decryption($c_url,$token);
    $data['data']['link']=$decoded;
    
    echo json_encode(['status'=>true,'message'=>'url listed','data'=>$data['data']]);
    die;
}else{
    echo json_encode(['status'=>false,'message'=>'url not found','data'=>[]]);
    die;
}

?>