<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <link href="./assets/css/video-js.css" rel="stylesheet" /><!--v_7.10.2-->
        <!-- City -->
    <link href="./assets/css/themes/city.css" rel="stylesheet">

    <!-- Fantasy -->
    <link href="./assets/css/themes/fantasy.css" rel="stylesheet">

    <!-- Forest -->
    <link href="./assets/css/themes/forest.css" rel="stylesheet">

    <!-- Sea -->
    <link href="./assets/css/themes/forest.css" rel="stylesheet">
    
    <title>DRM Player</title>
  </head>
  <body>
    <video autoplay controls id="player" class="vjs-theme-fantasy video-js"></video>
    <script src="./assets/js/jquery.min.js"></script>
    <script src="./assets/js/video.min.js"></script><!--v_7.10.2-->
    <script src="./assets/js/dash.all.debug.js"></script>
<!--    <script src='https://vjs.zencdn.net/7.5.4/video.js'></script>
    <script src="https://cdn.dashjs.org/latest/dash.all.min.js"></script>-->
    <script src="./assets/js/videojs-dash.js"></script>
    <script type="text/javascript">
        const queryString = window.location.search;

        const urlParams = new URLSearchParams(queryString);

        const token = urlParams.get('id')
        const base=window.location;
        
        $(document).ready(function(){
            $.ajax({
                url:base['origin'] + base['pathname'] + 'main.php',
                method:'post',
                dataType:'json',
                data:{token:token},
                success:function(res){
                    var DASH_MANIFEST_URL=res.data.link;
                    var SERVER_URL='https://license.pallycon.com/ri/licenseManager.do';
                    var TOKKEN=res.data.token;

                    var player = videojs('player',{
                        html5: {
                            nativeCaptions: false,
                            dash: {
                                    //setLimitBitrateByPortal: false,
                                    // setMaxAllowedBitrateFor: ['video', 2000]
                                }
                        },techOrder: ["html5","flash","youtube"] 
                    });
                    //player.addClass('vjs-matrix');
                    player.src([{
                        src:DASH_MANIFEST_URL,
                        type: 'application/dash+xml',
                        keySystemOptions: [
                            {
                                name: 'com.widevine.alpha',
                                options: {
                                    serverURL: SERVER_URL,
                                    httpRequestHeaders: {
                                          'pallycon-customdata-v2': TOKKEN
                                    }
                                }
                            }
                        ]
                    }]); 
                    //player.play();
                }
            });
        });
    </script>
  </body>
</html>
